<?php

/**
 * @file
 * Functions regarding tokens.
 */

/**
 * Implements hook_token_info().
 */
function webform_linkedin_tokens_token_info() {

  // Define the token type.
  $types['webform-linkedin'] = [
    'name' => t('Webform LinkedIn'),
    'description' => t('Tokens acquired through the linkedin API.'),
  ];

  /*
   * Simple key-value token types.
   * Construct LinkedIn token array which we can loop through to shorten the
   * amount of code.
   */
  $linkedin_tokens = [
    'id' => 'id',
    'first-name' => 'First name',
    'last-name' => 'Last name',
    'maiden-name' => 'Maiden name',
    'formatted-name' => 'Formatted name',
    'phonetic-first-name' => 'Phonetic first name',
    'phonetic-last-name' => 'Phonetic last name',
    'formatted-phonetic-name' => 'Formatted phonetic name',
    'headline' => 'Headline',
    'industry' => 'Industry',
    'current-share' => 'Current share',
    'num-connections' => 'Number of connections',
    'num-connections-capped' => 'Number of connections capped',
    'summary' => 'Summary',
    'specialties' => 'Specialties',
    'picture-url' => 'Picture URL',
    'picture-urls' => "picture URL's",
    'site-standard-profile-request' => 'Site standard profile request',
    'public-profile-url' => 'Public profile URL',
    'email-address' => 'E-mail address',
  ];

  // Tokens for linkedin webform.
  foreach ($linkedin_tokens as $tkey => $tvalue) {
    $tokens['webform-linkedin'][$tkey] = [
      'name' => t('@tvalue', ['@tvalue' => $tvalue]),
      'description' => t('The @tvalue LinkedIn token', ['@tvalue', $tvalue]),
    ];
  }

  // Object tokens. These are tokens which contain data returned by LinkedIn
  // as an object.
  // Location.
  $types['location'] = [
    'name' => t('Location'),
    'description' => t('Webform LinkedIn location tokens.'),
    'needs-data' => 'webform-linkedin',
  ];

  $tokens['webform-linkedin']['location'] = [
    'name' => t('Location'),
    'description' => t('Location'),
    'type' => 'location',
  ];

  $tokens['location']['name'] = [
    'name' => t('Location name'),
    'description' => t('Location name'),
  ];

  $tokens['location']['country:code'] = [
    'name' => t('Location country code'),
    'description' => t('Location country code'),
  ];

  // currentShare.
  $types['current-share'] = [
    'name' => t('Current Share'),
    'description' => t('The most recent item the member has shared on LinkedIn.'),
    'needs-data' => 'webform-linkedin',
  ];

  $tokens['webform-linkedin']['current-share'] = [
    'name' => t('Current Share'),
    'description' => t('Current Share'),
    'type' => 'current-share',
  ];

  // Creating a loop to shorten the code.
  $content_tokens = [
    'author:first-name' => 'Author first name',
    'author:last-name' => 'Author last name',
    'comment' => 'Comment',
    'content:description' => 'Description',
    'content:eyebrow-url' => 'Eyebrow URL',
    'content:resolved-url' => 'Resolved URL',
    'content:shortened-url' => 'Short URL',
    'content:submitted-image-url' => 'Submitted image URL',
    'content:submitted-url' => 'Submitted URL',
    'content:thumbnail-url' => 'Thumbnail URL',
    'content:title' => 'Title',
    'id' => 'ID',
    'visibility' => 'Visibility:code',
  ];

  foreach ($content_tokens as $key => $content_token) {
    $tokens['current-share'][$key] = [
      'name' => t('@content_token', ['@content_token' => $content_token]),
      'description' => t('@content_token', ['@content_token', $content_token]),
    ];
  }

  // Positions.
  $types['positions'] = [
    'name' => t('Positions'),
    'description' => t("The member's current positions."),
    'needs-data' => 'webform-linkedin',
  ];

  $tokens['webform-linkedin']['positions'] = [
    'name' => t('Positions'),
    'description' =>
    t("Current positions. Provide the delta at the '?' to print a specific
      field. E.G. [webform-linkedin:values:0:company:name] will print the
      company name for the first position."),
    'type' => 'positions',
  ];

  $tokens['positions']['all-positions'] = [
    'name' => t('All positions'),
    'description' => t('Will output all fields for all posiotions.'),
  ];

  $postion_tokens = [
    'values:?:company:id' => 'Company ID',
    'values:?:company:industry' => 'Company industry',
    'values:?:company:name' => 'Company name',
    'values:?:company:size' => 'Company size',
    'values:?:company:type' => 'Company type',
    'values:?:id' => 'ID',
    'values:?:is-current' => 'Is Current',
    'values:?:location:name' => 'Location name',
    'values:?:start-date:month' => 'Start date month',
    'values:?:start-date:year' => 'Start date year',
    'values:?:summary' => 'Summary',
    'values:?:title' => 'Title',
  ];
  foreach ($postion_tokens as $key => $postion_token) {
    $tokens['positions'][$key] = [
      'name' => t('@postion_token', ['@postion_token' => $postion_token]),
      'description' => t('@postion_token', ['@postion_token', $postion_token]),
    ];
  }

  // Return the values.
  return [
    'types' => $types,
    'tokens' => $tokens,
  ];
}

/**
 * Implements hook_tokens().
 */
function webform_linkedin_tokens_tokens($type, $tokens, $data = [], $options = []) {
  $replacements = [];

  if ($type == 'webform-linkedin') {
    // Fetch all tokens in this webform so we only need one request.
    // Check if were coming from a redirect from linkedin.
    if (isset($_GET['code'])) {
      // We don't want to make a request if we do not have an access token.
      if (!isset($_SESSION['linkedin'][$data['node']->nid]['access_token'])) {
        $settings = _webform_linkedin_tokens_settings($data['node']->nid);
        // Only make a request if getAccessToken is successful.
        if (webform_linkedin_tokens_get_access_token($settings)) {
          $field_tokens = _webform_linkedin_tokens_fetch_tokens($data['node']->webform['components']);
          $field_tokens = implode(',', _webform_linkedin_tokens_create_request_tokens($field_tokens));
          $object = webform_linkedin_tokens_fetch('/v1/people/~:(' . $field_tokens . ')', $data['node']->nid);
          $_SESSION['linkedin'][$data['node']->nid]['data'] = $object;
        }
      }
    }

    // Go ahead with token replacement.
    if (isset($_SESSION['linkedin'][$data['node']->nid]['data'])) {
      foreach ($tokens as $name => $original) {
        $linkedinkey = _webform_linkedin_tokens_camelcase($name);
        if (isset($_SESSION['linkedin'][$data['node']->nid]['data']->{$linkedinkey})) {
          if (!is_object($_SESSION['linkedin'][$data['node']->nid]['data']->{$linkedinkey})) {
            // LinkedIn returned this key with a value,
            // so we can easily replace it.
            $replacements[$original] = $_SESSION['linkedin'][$data['node']->nid]['data']->{$linkedinkey};
          }
        }
        elseif (strpos($linkedinkey, ':')) {
          // This is a custom nested token, we need to construct the object.
          // Create array of properties.
          $arrKeys = explode(':', $linkedinkey);
          if ($linkedinkey == 'positions:allPositions') {
            $replacements[$original] = _webform_linkedin_tokens_all_positions_output($_SESSION['linkedin'][$data['node']->nid]['data']->positions->values);
          }
          else {
            // Default nested tokens represent the LinkedIn object.
            $value = $_SESSION['linkedin'][$data['node']->nid]['data'];
            foreach ($arrKeys as $arrKey) {
              if (is_object($value)) {
                $value = $value->$arrKey;
              }
              elseif (is_array($value)) {
                $value = $value[$arrKey];
              }
            }
            $replacements[$original] = $value;
          }
        }
      }
    }
  }
  return $replacements;
}

/**
 * Helper function to make the LinkedIn request.
 */
function _webform_linkedin_tokens_create_request_tokens($tokens) {
  foreach ($tokens as $nr => $token) {
    if (strpos($token, ':')) {
      $token_string = explode(':', $token);
      if (!in_array($token_string[0], $tokens)) {
        $tokens[] = $token_string[0];
      }
      unset($tokens[$nr]);
    }
  }
  return $tokens;
}

/**
 * Helper function to change request key's to camelcase request keys's.
 */
function _webform_linkedin_tokens_camelcase($key) {
  $str = str_replace('-', ' ', $key);
  $str = trim($str);
  // Uppercase the first character of each word.
  $str = ucwords($str);
  $str = str_replace(' ', '', $str);
  $str = lcfirst($str);
  return $str;
}

/**
 * Helper function to generate output for all positions.
 */
function _webform_linkedin_tokens_all_positions_output($positions) {
  $output = '';
  // Loop through all positions.
  foreach ($positions as $key => $position) {
    $output .= 'Position:' . ($key + 1) . PHP_EOL . PHP_EOL;
    // Loop through all position fields.
    foreach ($position as $key => $value) {
      if (is_object($value)) {
        foreach ($value as $key2 => $value2) {
          // Populate output for all postiotions & all fields.
          $output .= $key2 . ': ' . $value2 . PHP_EOL;
        }
      }
      else {
        $output .= $key . ': ' . $value . PHP_EOL;
      }
    }
  }
  return $output;
}
