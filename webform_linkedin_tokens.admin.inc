<?php

/**
 * @file
 * Admin form.
 */

/**
 * Menu callback. Builds the admin form.
 */
function webform_linkedin_tokens_form($form, &$form_state, $node) {
  $form = [];

  $settings = _webform_linkedin_tokens_settings($node->nid);

  $form['webform_linkedin_tokens'] = [
    '#type' => 'fieldset',
    '#title' => t('Linkedin Token settings'),
    '#collapsible' => 1,
    '#collapsed' => FALSE,
    '#weight' => 10,
    '#tree' => TRUE,
  ];

  $form['webform_linkedin_tokens']['enable'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable'),
    '#description' => t('Enable LinkedIn Tokens for this webform.'),
    '#default_value' => !empty($settings) && $settings['enabled'] ? 1 : 0,
  ];

  $form['webform_linkedin_tokens']['container'] = [
    '#type' => 'container',
    '#states' => [
      'visible' => [':input[name="webform_linkedin_tokens[enable]"]' => ['checked' => TRUE]],
    ],
    '#parents' => ['webform_linkedin_tokens'],
  ];

  $form['webform_linkedin_tokens']['container']['api_key'] = [
    '#type' => 'textfield',
    '#title' => t('Api key'),
    '#description' => t('LinkedIn Client ID / Api key. Copy this from your LinkedIn application.'),
    '#default_value' => $settings['api_key'],
  ];

  $form['webform_linkedin_tokens']['container']['api_secret'] = [
    '#type' => 'textfield',
    '#title' => t('Api secret'),
    '#description' => t('LinkedIn Client Secret / Api secret. Copy this from your LinkedIn application.'),
    '#default_value' => $settings['api_secret'],
  ];

  $form['webform_linkedin_tokens']['container']['scope'] = [
    '#type' => 'textfield',
    '#title' => t('Scope'),
    '#description' =>
    t('LinkedIn default application permissions. Copy this from your LinkedIn application. Please make sure you request for the correct scope. See @linkedin for more info. Separate scopes with a whitespace.',
      ['@linkedin', l(t('LinkedIn Oauth Docs'),
        'https://developer.linkedin.com/docs/oauth2'),
      ]
    ),
    '#default_value' => $settings['scope'],
  ];

  global $base_url;

  $form['webform_linkedin_tokens']['container']['url_callback'] = [
    '#markup' =>
    t('Please add the following URL to the Authorized Redirect URLs: in your LinkedIn app settings: @linkedin', ['@linkedin' => $base_url . url('/node/' . $node->nid)]),
  ];

  $form['webform_linkedin_tokens']['container']['button'] = [
    '#type' => 'fieldset',
    '#title' => t('Button settings'),
    '#collapsible' => 1,
    '#collapsed' => FALSE,
    '#weight' => 10,
    '#tree' => TRUE,
  ];

  $form['webform_linkedin_tokens']['container']['button']['button_enable'] = [
    '#type' => 'checkbox',
    '#title' => t('Include button'),
    '#description' => t('Include "Login" and "Logout" button in this webform.'),
    '#default_value' => !empty($settings) && $settings['button_enabled'] ? 1 : 0,
  ];

  $form['webform_linkedin_tokens']['container']['button']['container2'] = [
    '#type' => 'container',
    '#states' => [
      'visible' => [':input[name="webform_linkedin_tokens[button][button_enable]"]' => ['checked' => TRUE]],
    ],
    '#parents' => ['button'],
  ];

  $form['webform_linkedin_tokens']['container']['button']['container2']['login_text'] = [
    '#type' => 'textfield',
    '#title' => t('Login text'),
    '#description' => t('The text to appear in the LinkedIn login button.'),
    '#default_value' => $settings['login_text'],
  ];

  $form['webform_linkedin_tokens']['container']['button']['container2']['logout_text'] = [
    '#type' => 'textfield',
    '#title' => t('Logout text'),
    '#description' => t('The text to appear in the LinkedIn logout button.'),
    '#default_value' => $settings['logout_text'],
  ];

  $form['webform_linkedin_tokens']['#node']['nid'] = $node->nid;

  $form['webform_linkedin_tokens']['edit-submit'] = [
    '#type' => 'submit',
    '#value' => t('Save settings'),
    '#weight' => 99,
  ];

  return $form;
}

/**
 * Submit handler for saving webform_linkedin_tokens settings.
 */
function webform_linkedin_tokens_form_submit($form, &$form_state) {

  $current_setting = _webform_linkedin_tokens_settings($form['webform_linkedin_tokens']['#node']['nid']);

  $record = [
    'nid' => $form['webform_linkedin_tokens']['#node']['nid'],
    'enabled' => $form_state['values']['webform_linkedin_tokens']['enable'],
    'api_key' => $form_state['values']['webform_linkedin_tokens']['api_key'],
    'api_secret' => $form_state['values']['webform_linkedin_tokens']['api_secret'],
    'scope' => $form_state['values']['webform_linkedin_tokens']['scope'],
    'button_enabled' => $form_state['values']['webform_linkedin_tokens']['button']['button_enable'],
    'login_text' => $form_state['values']['button']['login_text'],
    'logout_text' => $form_state['values']['button']['logout_text'],
  ];

  // Insert or update the settings of this webform.
  drupal_write_record('webform_linkedin_tokens', $record, empty($current_setting) ? [] : 'nid');

  drupal_set_message(t('Settings saved.'));
}
