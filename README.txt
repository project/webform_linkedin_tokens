CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------

Webform LinkedIn Tokens is a light weight module and an extension for the
Webform module, to populate webform fields with user profile data from LinkedIn
through tokens. Use cases for this module could be:

 * Supplement fields in a job application webform with data from LinkedIn.
 * Supplement fields in a commerce webform with data from LinkedIn.
 
What does this module?
 * It provides Tokens based on the LinkedIn REST API.
  (https://developer.linkedin.com/docs)
 * It allows to prefill a webform based on these tokens.
 * It offers an extra admin page attached to the webform to manage the
   connection to your LinkedIn app.
 * It provides a button which can be used to start the LinkedIn Oauth process.

What this module does not?
 * This module does nothing with Drupal user profiles fields
 * This module does not offer a service to log in to Drupal through a LinkedIn
   account.

REQUIREMENTS
------------

This module requires the following modules:

 * Token (https://www.drupal.org/project/token)
 * Webform (https://www.drupal.org/project/webform)

RECOMMENDED MODULES
-------------------

 * LinkedIn Integration.(https://www.drupal.org/project/linkedin). Provides 
   authentication through Linkedin. Provides data sync between user
   profiles & LinkedIn.
 * LinkedIn Group Posts. (https://www.drupal.org/project/linkedin_group_posts).
   Used to display LinkedIn posts in a specific group.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 * Create a LinkedIn app (https://www.linkedin.com/developer/apps). Be sure
   to check the scopes you which to use.
 * Create a webform.
 * Add the URI to the webform to the "Authorized Redirect URLs" in your
   LinkedIn App (Also explained at the admin page).
 * Go to the webform settings sub page.
 * Configure the LinkedIn api connection.
 * Configure which tokens (scope) you want to use. (LinkedIn will ask the user
   for permission).

CONFIGURATION
-------------
 
 * Configure user permissions in Administration » People » Permissions:
   - Administer Webform LinkedIn Tokens settings.
 * Configure the Webform LinkedIn Token settings on th Webform » LinkedIn Token
   settings tab.

TROUBLESHOOTING
---------------

 * When a linkedin app is newly created, it might take some time to become 
   active. If a message appears, saying connection to LinkedIn is not 
   possible, please check the settings, wait for a while, and try again.

MAINTAINERS
-----------

Current maintainers:
 * Kobus Beljaars (https://www.drupal.org/u/beljaako)
 
This project has been sponsored by:
 * ABLERZ (https://www.ablerz.com)
   Specialized in Drupal development, maintenance and consulting for Drupal. 
